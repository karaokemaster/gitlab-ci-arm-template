# gitlab-ci-arm-template

This repository contains an Multiarch CI template (tested for Arm and x86)

It uses the standard gitlab templates but overrides build rules in order to use buildx to build multi-arch docker

You can copy or include the template directly in your own .gitlab-ci.yml and override jobs, variables and/or functions to match your individual needs.

## Variables

* CI_BUILDX_ARCHS (Optional) - which architectures to build image for (linux/amd64, linux/arm64, etc.)
* CI_BUILD_ARM64 (Optional) - use a native arm64 gitlab-runner to build
* CI_BUILD_ARM (Optional) - use a native 32-bit arm gitlab-runner to build
* CI_AMD64_DISABLE (Optional) - don't build for amd64

Note: If CI_BUILDX_ARCHS is set, it will override native builders.
